# zinit
declare -A ZINIT
ZINIT[HOME_DIR]=~/.zinit
if [[ ! -d $ZINIT[HOME_DIR] ]]; then
    mkdir -p $ZINIT[HOME_DIR] && \
        git clone --depth 1 \
        https://github.com/zdharma/zinit.git \
        $ZINIT[HOME_DIR]/bin
fi
command_exists () { (( $+commands[$1] )); }
maybe () { [[ -f $2 ]] && $1 $2 2> /dev/null || true; }
maybe source $ZINIT[HOME_DIR]/bin/zinit.zsh
maybe source ~/.scratchrc

# programs
zinit silent wait light-mode from:gh-r as:program for \
    mv:'bat-**/bat -> bat' @sharkdp/bat \
    mv:'exa-* -> exa' @ogham/exa \
    @junegunn/fzf-bin

# zsh
zinit silent wait light-mode depth:1 for \
    multisrc:'shell/*.zsh' @junegunn/fzf \
    @ael-code/zsh-colored-man-pages \
    @hlissner/zsh-autopair \
    @le0me55i/zsh-extract \
    @mollifier/cd-gitroot \
    @jreese/zsh-titles \
    @hkupty/ssh-agent \
    @Aloxaf/fzf-tab \
    @agkozak/zsh-z \
    @zpm-zsh/ls

# fish
zinit silent wait light-mode depth:1 for \
    @zdharma/fast-syntax-highlighting \
    @zsh-users/zsh-autosuggestions \
    @zsh-users/zsh-completions

# rice
BAT_THEME='base16'
FZF_DEFAULT_OPTS="${FZF_DEFAULT_OPTS}
--pointer=➜ --prompt='➜ ' --marker=●
--color pointer:0,marker:0,border:8"
maybe cat ~/.cache/wal/sequences
maybe source ~/.cache/wal/colors.sh
zinit light denysdovhan/spaceship-prompt

# completion
zstyle ':completion:*' rehash true
zstyle ':completion:*:descriptions' format '[%d]'
zstyle ':completion:*' matcher-list 'm:{[:lower:]}={[:upper:]}'
autoload -Uz compinit && compinit
zinit cdreplay -q

# history
HISTSIZE=10000
SAVEHIST=10000
HISTFILE=~/.zhistory
setopt extendedhistory \
    incappendhistory \
    histreduceblanks \
    histignorealldups \
    histexpiredupsfirst

# dirs
setopt autocd \
    autopushd \
    pushdignoredups

# env
export GOPATH=~/go
# export WORDCHARS=':'
export WORDCHARS='_-*?[]~&.;!#$%^(){}<>'
export PAGER=less
export EDITOR='emacsclient -c'
path=(
    ~/.yarn/bin
    ~/.cargo/bin
    ~/.luarocks/bin
    ~/.local/bin
    ~/.emacs.d/bin
    ~/.config/kak/bin
    ~/.Xilinx/Vitis/2019.2/bin
    $GOPATH/bin
    ~/flutter/bin
    ~/Android/Sdk/tools/bin
    ~/Android/Sdk/emulator
    $path
)
export PATH

# aliases
alias e=emacs
alias ipy=ipython
alias cdg=cd-gitroot
[ "$TERM" = 'xterm-kitty' ] && alias ssh='kitty +kitten ssh'
command_exists xdg-open && alias o=xdg-open
if command_exists systemctl; then
    alias sv='sudo systemctl'
    alias svu='systemctl --user'
fi

# functions
FZF_WINDOW_PARAMS='right:66%:wrap'
fzfy () {
    local list=$1
    local preview=$2
    shift 2
    eval $list |
        fzf -m --preview $preview \
            --preview-window $FZF_WINDOW_PARAMS |
        xargs -ro $@
}

if command_exists pamac; then
    alias ai='pamac install --no-confirm'
    alias au='pamac update --no-confirm'
    alias ar='pamac remove --no-confirm'
    as () {
        fzfy 'pamac list -rq `pamac list -r`' \
            'pamac info {1}' pamac install --no-confirm;
    }
fi

sw () {
    local id=$(xdo id)
    xdo hide
    ("$@" > /dev/null 2>&1)
    xdo show "$id"
}

ec () { sw emacsclient -c $@; }
ie () {
    git clone https://github.com/hlissner/doom-emacs ~/.emacs.d
    git clone https://gitlab.com/jjzmajic/doom ~/.doom.d
    doom sync
}

embedded_compdef () {
    local cmd=$1
    local expr=$2
    local compfile=$ZINIT[COMPLETIONS_DIR]/_$1
    if command_exists $cmd; then
        ! [[ -e $compfile ]] &&
            eval $expr >! $compfile
    fi
}

source_bash () {
    emulate -L bash
    builtin source "$@"
}

# external
embedded_compdef kubectl 'kubectl completion zsh'
embedded_compdef helm 'helm completion zsh'
embedded_compdef bw 'bw completion --shell zsh'
command_exists direnv && eval "$(direnv hook zsh)"
command_exists aws_zsh_completer.sh && source `which aws_zsh_completer.sh`

if command_exists aircrack-ng; then
    WIFI=wlp0s20f0u2
    alias monitor='sudo airmon-ng start ${WIFI}'
    alias umonitor='sudo airmon-ng stop ${WIFI}mon'
    alias dump='sudo airodump-ng -w dump ${WIFI}mon'
    deauth () { sudo aireplay-ng --deauth 5 -a "$1" -c "$2" ${WIFI}mon; }
    listen () { sudo airodump-ng --bssid "$1" -c "$2" ${WIFI}mon; }
fi

if [[ $INSIDE_EMACS = vterm ]]; then
    vterm_printf() {
        if [[ -n "$TMUX" ]]; then
            printf "\ePtmux;\e\e]%s\007\e\\" "$1"
        elif [[ "${TERM%%-*}" = "screen" ]]; then
            printf "\eP\e]%s\007\e\\" "$1"
        else
            printf "\e]%s\e\\" "$1"
        fi
    }
    alias clear='vterm_printf "51;Evterm-clear-scrollback";tput clear'
    setopt promptsubst
    vterm_prompt_end() { vterm_printf "51;A$(whoami)@$(hostname):$(pwd)"; }
    PROMPT=$PROMPT'%{$(vterm_prompt_end)%}'
fi

# Local Variables:
# mode: shell-script
# End:
